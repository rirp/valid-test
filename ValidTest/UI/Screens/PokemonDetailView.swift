//
//  DetailView.swift
//  ValidTest
//
//  Created by Ronald Ivan Ruiz Poveda on 23/02/20.
//  Copyright © 2020 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import SwiftUI

struct PokemonDetailView: View {
    @ObservedObject var viewModel: PokemonDetailViewModel
    @ObservedObject var fetcher = ImageFetcher()
    
    init(pokemon: Pokemon) {
        self.viewModel = PokemonDetailViewModel(pokemon: pokemon)
    }
    
    var body: some View {
        VStack {
            Image("detail_header")
                .resizable()
                .clipped()
                .edgesIgnoringSafeArea(.top)
                .frame(height: 200)
            
            CircleImage(image: fetcher.data == nil ? Image("nav_background") : Image(uiImage: UIImage(data: fetcher.data!)!))
                .offset(y: -80.0)
                .padding(.bottom, -80)

            VStack(alignment: .center, spacing: 20  ) {
                Text(viewModel.detail?.name != nil ? viewModel.detail!.name : "")
                    .font(.title)
                
                Text("Aspect")
                    .font(.headline)
                HStack {
                    HStack {
                        Text("Height: ")
                            .fontWeight(.black)
                        Text("\(viewModel.detail?.height ?? 0) m")
                    }
                    HStack {
                        Text("Weight: ")
                            .fontWeight(.black)
                        Text("\(viewModel.detail?.weight ?? 0) kg")
                    }
                    
                }

            }
            .padding()

            Spacer()
        }.onAppear {
            self.viewModel.getDetail()
            self.fetcher.getImage(id: self.viewModel.pokemon.id!)
        }
    }
}

struct PokemonDetailView_Previews: PreviewProvider {
    static var previews: some View {
        PokemonDetailView(pokemon: Pokemon(id: 0, name: "ASdfsd", image: Data()))
    }
}
