//
//  PokemonList.swift
//  ValidTest
//
//  Created by Ronald Ivan Ruiz Poveda on 23/02/20.
//  Copyright © 2020 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import SwiftUI

struct PokemonList: View {
    @State private var searchText = ""
    @ObservedObject var viewModel = PokemonViewModel()
    
    var body: some View {
        LoadingView(isShowing: .constant(self.viewModel.isShowingLoading)) {
            NavigationView {
                ZStack {
                    Image(uiImage: UIImage(named: "nav_background")!)
                        .resizable()
                        .clipped()
                        .blur(radius: 30)
                        .edgesIgnoringSafeArea(.all)
                    VStack(spacing: 0) {
                        SearchBar(text: self.$searchText)
                        List(self.viewModel.filter(by: self.searchText)) { pokemon in
                            NavigationLink(destination: PokemonDetailView(pokemon: pokemon)) {
                                PokemonRow(pokemon: pokemon)
                            }
                        }
                        .listRowBackground(Color(red: 0, green: 191, blue: 255))
                        .onAppear {
                            if self.viewModel.items.isEmpty {
                                self.viewModel.getPokemon()
                            } else {
                                //self.viewModel.isShowingLoading = false
                            }
                        }
                    }
                }
                .navigationBarTitle("Valid").accentColor(Color.white)
            }
            .accentColor( .white)
        }
    }
}

struct PokemonList_Previews: PreviewProvider {
    static var previews: some View {
        PokemonList()
    }
}
