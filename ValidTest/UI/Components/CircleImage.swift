//
//  CircleImage.swift
//  ValidTest
//
//  Created by Ronald Ivan Ruiz Poveda on 23/02/20.
//  Copyright © 2020 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import SwiftUI

struct CircleImage: View {
    @State var animationAmount: CGFloat = 1
    var image: Image?

    var body: some View {
        image?.frame(width: 150, height: 150)
            .clipShape(Circle())
            .overlay(
                Circle().stroke(Color.blue, lineWidth: 1)
                .scaleEffect(animationAmount)
                .opacity(Double(2 - animationAmount))
                .animation(
                    Animation.easeInOut(duration: 1)
                    .repeatForever(autoreverses: false)
                )
            )
            .shadow(radius: 10)
            .background(Circle().fill(Color.white))
            .onAppear {
                self.animationAmount = 2
            }
    }
}

struct CircleImage_Preview: PreviewProvider {
    static var previews: some View {
        CircleImage(image: Image("nav_background"))
    }
}
