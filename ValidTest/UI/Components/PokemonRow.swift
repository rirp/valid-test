//
//  PokemonRow.swift
//  ValidTest
//
//  Created by Ronald Ivan Ruiz Poveda on 23/02/20.
//  Copyright © 2020 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import SwiftUI

struct PokemonRow: View {
    var pokemon: Pokemon
    @ObservedObject var fetcher = ImageFetcher()
    
    init(pokemon: Pokemon) {
        self.pokemon = pokemon
    }
    
    var body: some View {
        HStack {
            LoadableImageView(with: self.fetcher.data)
                .frame(width: 80.0, height: 80.0)
            VStack(alignment: .leading) {
                Text(pokemon.name).font(.headline)
                Text(String(format: "%03d", pokemon.id!)).font(.subheadline)
            }
        }.onAppear {
            self.fetcher.getImage(id: self.pokemon.id!)
        }
    }
}
