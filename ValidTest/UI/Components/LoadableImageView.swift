//
//  LoadableImageView.swift
//  ValidTest
//
//  Created by Ronald Ivan Ruiz Poveda on 23/02/20.
//  Copyright © 2020 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import SwiftUI

struct LoadableImageView: View {
    private var data: Data?
    
    var stateContent: AnyView {
        if let data = data, let image = UIImage(data: data) {
            return AnyView(
                Image(uiImage: image).resizable()
            )
        } else {
            return AnyView(
                ActivityIndicator(isAnimating: .constant(true), style: .medium)
            )
        }
    }
    
    init(with data: Data?) {
        self.data = data
    }
    
    func image(from data:Data) -> UIImage {
        UIImage(data: data) ?? UIImage()
    }
    
    var body: some View {
        HStack {
            stateContent
        }
    }
}

#if DEBUG
struct LoadableImageView_Previews : PreviewProvider {
    static var previews: some View {
        LoadableImageView(with: Data())
    }
}
#endif
