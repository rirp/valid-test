//
//  Pokemon.swift
//  ValidTest
//
//  Created by Ronald Ivan Ruiz Poveda on 23/02/20.
//  Copyright © 2020 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation

struct PokemonResponse: Codable {
    let count: Int
    let results: [Pokemon]
}

struct Pokemon: Identifiable, Hashable, Codable {
    var id: Int?
    let name: String
    let image: Data?
}

struct Type: Codable {
    var name: String
}

struct Types: Codable {
    var type: [Type]
}

struct PokemonDetail: Codable {
    var name: String
    var height: Int
    var weight: Int
    var baseExperience: Int
    
    enum CodingKeys: String, CodingKey {
        case name, height, weight
        case baseExperience = "base_experience"
    }
}
