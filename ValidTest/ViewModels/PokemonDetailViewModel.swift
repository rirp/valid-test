//
//  PokemonDetailViewModel.swift
//  ValidTest
//
//  Created by Ronald Ivan Ruiz Poveda on 23/02/20.
//  Copyright © 2020 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation

import SwiftUI

class PokemonDetailViewModel: ObservableObject {
    @Published var detail: PokemonDetail?
    var pokemon: Pokemon
    
    init(pokemon: Pokemon) {
        self.pokemon = pokemon
    }
    
    func getDetail() {
        let url = "\(APIManager.detail)\(pokemon.id!)"
        APIManager.requestData(url: url) { response in
            switch(response) {
                case .success(let data):
                    self.detail = try! JSONDecoder().decode(PokemonDetail.self, from: data)
                case .failure(let failure):
                    print(failure)
            }
        }
    }
    
    func getImage() {
        
    }
}
