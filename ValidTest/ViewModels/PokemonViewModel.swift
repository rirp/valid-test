//
//  PokemonListInteractor.swift
//  ValidTest
//
//  Created by Ronald Ivan Ruiz Poveda on 23/02/20.
//  Copyright © 2020 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import Foundation
import SwiftUI

class PokemonViewModel: ObservableObject {
    @Published var items: [Pokemon] = []
    @Published var isShowingLoading = true

    func filter(by name: String) -> [Pokemon] {
        return self.items.filter {
            name.isEmpty ? true : $0.name.lowercased().contains(name.lowercased())
        }
    }
    
    func getPokemon() {
        self.isShowingLoading = true
        APIManager.requestData(url: APIManager.baseUrl) { (response) in
            switch(response) {
                case .success(let data):
                    self.getObject(from: data)
                case .failure(let failure):
                    print(failure)
            }
            
        }
    }
    
    private func getObject(from data: Data) {
            self.isShowingLoading = false
        let items = try! JSONDecoder().decode(PokemonResponse.self, from: data).results
        self.items = items.enumerated().map { (index, element) in
            return Pokemon(id: index + 1, name: element.name, image: nil)
        }
    }
    
}
