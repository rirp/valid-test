//
//  RemoteImageURL.swift
//  ValidTest
//
//  Created by Ronald Ivan Ruiz Poveda on 23/02/20.
//  Copyright © 2020 Ronald Ivan Ruiz Poveda. All rights reserved.
//

import SwiftUI

class ImageFetcher: ObservableObject {
    
    @Published private(set) var data: Data?
    
    func getImage(id: Int) {
        
        let url = "\(APIManager.baseImg)\(id).png"
        do {
            self.data = try Data(contentsOf: URL(string: url)!)
        } catch {
            print("Error")
        }
    }
    
    func cancel() {
        APIManager.cancel()
    }
}
